<?php


class DataBase
{
    private static $connection;

    public static function connect(){
        try{
            self::$connection = new PDO("mysql:host=localhost;dbname=pdo","admin","admin");
            $con = self::$connection;
            $con->exec("set names utf8");
            return $con;
        }catch (Exception $e){
            echo $e->getMessage();
        }
    }
}
DataBase::connect();
