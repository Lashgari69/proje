<?php

include_once "Controller.php";

$title = $_POST['title'];
$summary = $_POST['summary'];
$content = $_POST['content'];
$image = $_FILES['image'];
$id = $_POST['id'];

Controller::updateNews($title, $summary, $content, $image, $id);
header("location:showNews.php");