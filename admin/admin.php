<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <link rel="stylesheet" href="../front/css/bootstrap.min.css">
    <!--<link rel="stylesheet" href="css/bootstrap-reboot.min.css">-->
    <link rel="stylesheet" href="../front/css/custom.css">
    <title>صفحه ورود</title>
</head>
<body class="httm">
<!-- menu -->
<section class="container-fluid ">
    <section class="menu ">
        <section class="row">
            <section class="col-12 ">
                <nav class="navbar bg-warning  navbar-expand-sm fixed-top mr-3 ml-3 ">
                    <section class="container-fluid ">
                        <button class="navbar-toggler bg-primary fa fa-list " style="color: #c69500" data-toggle="collapse" data-target="#myToggleNav">

                        </button>
                        <section class="collapse navbar-collapse menuco " id="myToggleNav">
                            <section class="navbar-nav w-100 justify-content-start flex-sm-row-reverse text-center">
                                <a href="admin.php" class="nav-link nav-item active">تنظیمات</a>
                                <section class="dropdown">
                                    <a href="#" class="nav-link nav-item dropdown-toggle" data-toggle="dropdown">اخبار</a>
                                    <section class="dropdown-menu">
                                        <a href="showNews.php" class="dropdown-item text-right"> ویرایش</a>
                                        <a href="news.php" class="dropdown-item text-right"> اضافه کردن</a>

                                    </section>
                                </section>
                                 <a href="../logout.php" class="nav-link nav-item " style="float: left">خروج</a>
                            </section>
                        </section>
                    </section>

                </nav>
            </section>

        </section>
    </section>
</section>
<!-- end menu -->
<br><br><br>


<!-- footer -->

<section class="container-fluid ">
    <section class="row ">
        <section class=" col-8 offset-2 mt-3">
            <footer class="text-dark text-center">
                <hr>
                <p>Copyright &copy; 2019 Blessing.com<p>
            </footer>
        </section>
    </section>
</section>
<!-- end footer -->
<!-- locate js -->
<script src="../front/js/jquery-3.4.1.min.js"></script>
<script src="../front/js/bootstrap.min.js"></script>
<script src="../front/js/popper.min.js"></script>
<script src="../front/js/icon.js"></script>
</body>
</html>