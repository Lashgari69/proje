<?php
require_once "Controller.php";

$title = $_POST['title'];
$summary = $_POST['summary'];
$content = $_POST['content'];
$image = $_FILES['image'];

$image_new = Controller::uploadImage($image);
Controller::insertNews($title,$summary,$content,$image_new);
header("location:news.php");