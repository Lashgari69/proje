<?php

require_once "../DataBase.php";
session_start();

class Controller
{
    private static $con;

    public static function connect(){
        self::$con=DataBase::connect();
        $connection = self::$con;
        return $connection;
    }
    public static function uploadImage($image){
        $image_new = $image['name'];
        $upload = 1;
        $directory = "images/new/".$image_new;
        $filetype = pathinfo($directory, PATHINFO_EXTENSION);
        if ($filetype !== "jpg" && $filetype !== "png" && $filetype !== "gif") {
            $upload = 0;
        } elseif ($image['size'] > 5000000) {
            $upload = 0;
        }elseif (file_exists($directory)){
            $upload = 0;
            $_SESSION['image'] = "لطفا نام عکس خود را عوض کنید!";
        }
        elseif ($upload == 1) {
            move_uploaded_file($image['tmp_name'], $directory);
        }
        return $image_new;
    }
    public static function insertNews($title,$summary,$content,$image){
        $connection = self::connect();
        $query = $connection->prepare("insert into pdo.news(title, summary, content, image) value (:title,:summary,:content,:image)");
        $query->bindParam(':title',$title);
        $query->bindParam(':summary',$summary);
        $query->bindParam(':content',$content);
        $query->bindParam(':image',$image);
        $query->execute();
    }
    public static function showNews(){
        $connection = self::connect();
        $query = $connection->prepare("select * from pdo.news");
        $query->execute();
        $select = $query->fetchAll();
        return $select;
    }
    public  static function deleteNews($id){
        $connection = self::connect();
        $query = $connection->prepare("delete from pdo.news where id=:id");
        $query->bindParam(":id",$id);
        $query->execute();
    }
    public static function updateNews($title,$summary,$content,$image,$id){
        $connection = self::connect();
        $image_new = self::uploadImage($image);
        $query = $connection->prepare("update pdo.news set title=:title,summary=:summary,content=:content,image=:image,id=:id");
        $query->bindParam(":title",$title);
        $query->bindParam(":summary",$summary);
        $query->bindParam(":content",$content);
        $query->bindParam(":image",$image_new);
        $query->bindParam(":id",$id);
        $query->execute();
    }
    public static function selectById($id){
        $connection = self::connect();
        $query = $connection->prepare("select * from pdo.news where id=:id");
        $query->bindParam(":id",$id);
        $query->execute();
        return $query->fetch();
    }
}