<?php
require_once "Controller.php";

$id = $_REQUEST['id'];
$select = Controller::selectById($id);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../front/css/bootstrap.min.css">
    <!--<link rel="stylesheet" href="css/bootstrap-reboot.min.css">-->
    <link rel="stylesheet" href="../front/css/custom.css">
    <title>Document</title>
</head>
<body>
<!-- menu -->
<section class="container-fluid ">
    <section class="menu ">
        <section class="row">
            <section class="col-12 ">
                <nav class="navbar bg-warning  navbar-expand-sm  mr-3 ml-3 ">
                    <section class="container-fluid ">
                        <button class="navbar-toggler bg-primary fa fa-list " style="color: #c69500" data-toggle="collapse" data-target="#myToggleNav">

                        </button>
                        <section class="collapse navbar-collapse menuco " id="myToggleNav">
                            <section class="navbar-nav w-100 justify-content-start flex-sm-row-reverse text-center">
                                <a href="admin.php" class="nav-link nav-item active">تنظیمات</a>
                                <section class="dropdown">
                                    <a href="#" class="nav-link nav-item dropdown-toggle" data-toggle="dropdown">اخبار</a>
                                    <section class="dropdown-menu">
                                        <a href="showNews.php" class="dropdown-item text-right"> ویرایش</a>
                                        <a href="news.php" class="dropdown-item text-right"> اضافه کردن</a>

                                    </section>
                                </section>
                                <a href="../logout.php" class="nav-link nav-item " style="float: left">خروج</a>
                            </section>
                        </section>
                    </section>

                </nav>
            </section>

        </section>
    </section>
</section>
<!-- end menu -->
<section class="container mt-5">
    <section class="col-6 offset-3">
        <form action="updateFinal.php" method="post" enctype="multipart/form-data">
            <section class="form-group">
                <label for="title">title</label>
                <input type="text" id="title" class="form-control" value="<?php echo $select['title'];?>" name="title" style="border: 2px inset blue">
            </section>
            <section class="form-group">
                <label for="summary">summary</label>
                <input type="text" id="summary" class="form-control" value="<?php echo $select['summary'];?>" name="summary" style="border: 2px inset blue">
            </section>

            <section class="form-group">
                <label for="image">image</label>
                <input type="file" id="image" class="form-control" name="image" style="border: 2px inset blue" >
                <img src="images/new/<?php echo $select['image']; ?>" alt="" width="50px" height="50px">
                <?php if (isset($_SESSION['image'])): ?>
                    <h5><?php echo $_SESSION['image']; ?></h5>
                <?php endif; ?>
                <?php $_SESSION['image'] = null; ?>
            </section>
            <section class="form-group">
                <label for="content">content</label>
                <textarea id="content" class="form-control editor"name="content"  style="border: 2px inset blue; resize: none">
                    <?php echo $select['content'];?>
                </textarea>
                <input type="hidden" name="id" value="<?php echo $select['id'];?>">
            </section>
            <input type="submit" value="save" class="btn btn-warning btn-block">

        </form>
    </section>
</section>

<!-- js location -->
<script src="../front/js/jquery-3.4.1.min.js"></script>
<script src="../front/js/bootstrap.min.js"></script>
<script src="../front/js/popper.min.js"></script>
<script src="../front/ckeditor/ckeditor.js"></script>
<script src="../front/ckeditor/adapters/jquery.js"></script>
<script>
    $('textarea.editor').ckeditor();
</script>
</body>
</html>