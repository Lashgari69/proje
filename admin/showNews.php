<?php
include_once "Controller.php";

$select = Controller::showNews();
$item =[];
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../front/css/bootstrap.min.css">
    <!--<link rel="stylesheet" href="css/bootstrap-reboot.min.css">-->
    <link rel="stylesheet" href="../front/css/custom.css">
    <title>show</title>
</head>
<body>
<!-- menu -->
<section class="container-fluid ">
    <section class="menu ">
        <section class="row">
            <section class="col-12 ">
                <nav class="navbar bg-warning  navbar-expand-sm fixed-top mr-3 ml-3 ">
                    <section class="container-fluid ">
                        <button class="navbar-toggler bg-primary fa fa-list " style="color: #c69500" data-toggle="collapse" data-target="#myToggleNav">

                        </button>
                        <section class="collapse navbar-collapse menuco " id="myToggleNav">
                            <section class="navbar-nav w-100 justify-content-start flex-sm-row-reverse text-center">
                                <a href="admin.php" class="nav-link nav-item active">تنظیمات</a>
                                <section class="dropdown">
                                    <a href="#" class="nav-link nav-item dropdown-toggle" data-toggle="dropdown">اخبار</a>
                                    <section class="dropdown-menu">
                                        <a href="showNews.php" class="dropdown-item text-right"> ویرایش</a>
                                        <a href="news.php" class="dropdown-item text-right"> اضافه کردن</a>

                                    </section>
                                </section>
                                <a href="../logout.php" class="nav-link nav-item " style="float: left">خروج</a>
                            </section>
                        </section>
                    </section>

                </nav>
            </section>

        </section>
    </section>
</section>
<!-- end menu --><br>
<section class="container mt-5">
    <table class="table table-hover table-dark ">
        <thead>
        <tr>
            <th>id</th>
            <th>title</th>
            <th>image</th>
            <th>active</th>
            <th>delete</th>
            <th>update</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($select as $item): ?>
            <tr>
                <td><?php echo $item['id']; ?> </td>
                <td><?php echo $item['title']; ?> </td>
                <td><img src="images/new/<?php echo $item['image']; ?>" alt="" width="50px" height="50px"> </td>
                <td>
                    <?php if ($item['active']==0): ?>
                        <span class="badge badge-danger">غیرفعال</span>
                    <?php else: ?>
                        <span class="badge badge-success">فعال</span>
                    <?php endif; ?>
                </td>
                <td>
                    <form method="POST" action="deleteNews.php?id=<?php echo $item['id']; ?>&&image=<?php echo "images/new/".$item['image'];?>">
                        <input type="hidden" name="delete">
                        <input type="submit"  class="btn-outline-danger" value="delete">
                    </form>
                </td>
                <td>
                    <form method="POST" action="updateNews.php?id=<?php echo $item['id']; ?>">
                        <input type="hidden" name="update">
                        <input type="submit"  class="btn-outline-primary" value="update">
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</section>


<!-- js location -->
<script src="../front/js/jquery-3.4.1.min.js"></script>
<script src="../front/js/bootstrap.min.js"></script>
<script src="../front/js/popper.min.js"></script>
</body>
</html>
