<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <link rel="stylesheet" href="front/css/bootstrap.min.css">
    <!--<link rel="stylesheet" href="css/bootstrap-reboot.min.css">-->
    <link rel="stylesheet" href="front/css/custom.css">
    <title>صفحه ورود</title>
</head>
<body class="httm">
<!-- menu -->
<section class="container-fluid ">
    <section class="menu ">
        <section class="row">
            <section class="col-12 ">
                <nav class="navbar bg-warning  navbar-expand-sm fixed-top mr-3 ml-3 ">
                    <section class="container-fluid ">
                        <button class="navbar-toggler bg-primary fa fa-list " style="color: #c69500" data-toggle="collapse" data-target="#myToggleNav">

                        </button>
                        <section class="collapse navbar-collapse menuco " id="myToggleNav">
                            <section class="navbar-nav w-100 justify-content-start flex-sm-row-reverse text-center">
                                <a href="#" class="nav-link nav-item active">صفحه اصلی</a>
                                <a href="#" class="nav-link nav-item">درباه ما</a>
                                <a href="#" class="nav-link nav-item"> تماس با ما</a>
                                <a href="#" class="nav-link nav-item">گالری</a>
                                <a href="#" class="nav-link nav-item">اخبار</a>
                                <section class="dropdown">
                                    <a href="#" class="nav-link nav-item dropdown-toggle" data-toggle="dropdown">آموزش</a>
                                    <section class="dropdown-menu">
                                        <a href="#" class="dropdown-item text-right">برنامه نویسی</a>
                                        <a href="#" class="dropdown-item text-right">شبکه</a>
                                        <a href="#" class="dropdown-item text-right">امنیت</a>
                                    </section>
                                </section>
                                 <a href="login.php" class="nav-link nav-item " style="float: left">ورود</a>
                                 <a href="logout.php" class="nav-link nav-item " style="float: left">خروج</a>
                            </section>
                        </section>
                    </section>

                </nav>
            </section>

        </section>
    </section>
</section>
<!-- end menu -->
<br><br><br>
<!-- slide show -->
<section class="container-fluid ">
    <section class="row">
        <section class="col-12">
            <section id="demo" class="carousel slide "  data-ride="carousel">

                <!-- Indicators -->
                <ul class="carousel-indicators" >
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                    <li data-target="#demo" data-slide-to="3"></li>
                </ul>

                <!-- The slideshow -->
                <section class="carousel-inner " style="height: 500px">
                    <section class="carousel-item active">
                        <img src="image/img1.jpg" alt="Los Angeles">
                    </section>
                    <section class="carousel-item">
                        <img src="image/img2.jpg" alt="Chicago">
                    </section>
                    <section class="carousel-item">
                        <img src="image/img3.jpg" alt="New York">
                    </section>
                    <section class="carousel-item">
                        <img src="image/img4.jpg" alt="New York">
                    </section>
                </section>

                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>

            </section>
        </section>
    </section>
</section>
<!-- end slide show -->
<!-- start main body -->
<section class="container-fluid">
    <section class="row">
        <section class="col-sm-2  mt-2">
            <section>
                <header class="text-center"><b>اخبار</b></header>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam consequuntt amet, consectetur adipisicing elit. Aperiam consequuntt amet, consectetur adipisicing elit. Aperiam consequuntt amet, consectetur adipisicing elit. Aperiam consequuntt amet, consectetur adipisicing elit. Aperiam consequuntt amet, consectetur adipisicing elit. Aperiam consequuntt amet, consectetur adipisicing elit. Aperiam consequuntt amet, consectetur adipisicing elit. Aperiam consequuntur cum debitis dolore iure officia pariatur reprehenderit saepe tenetur veritatis. Autem beatae culpa distinctio facere id minima natus neque optio!</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam consequuntt amet, consectetur adipisicing elit. Aperiam consequuntt amet, consectetur adipisicing elit. Aperiam consequuntt amet, consectetur adipisicing elit. Aperiam consequuntt amet, consectetur adipisicing elit. Aperiam consequuntt amet, consectetur adipisicing elit. Aperiam consequuntt amet, consectetur adipisicing elit. Aperiam consequuntt amet, consectetur adipisicing elit. Aperiam consequuntur cum debitis dolore iure officia pariatur reprehenderit saepe tenetur veritatis. Autem beatae culpa distinctio facere id minima natus neque optio!</p>

			</section>
        </section>
        <section class="col-sm-8 mt-2">
            <section>
                <header class="text-center"><b>آموزش</b></header>
                <p >Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam consequut, consectetu adipisicing elit. Aperiam consequut, consectetu adipisicing elit. Aperiam consequut, consectetu adipisicing elit. Aperiam consequut, consectetu adipisicing elit. Aperiam consequut, consectetu adipisicing elit. Aperiam consequut, consectetu adipisicing elit. Aperiam consequut, consectetur adipisicing elit. Aperiam consequut, consectetur adipisicing elit. Aperiam consequuntur cum debitis dolore iure officia pariatur reprehenderit saepe tenetur veritatis. Autem beatae culpa distinctio facere id minima natus neque optio!</p>
            </section>
        </section>
        <section class="col-sm-2  mt-2">
            <section >
                <header class="text-center"><b>پروفایل</b></header>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam consequuntetur adipisicing elit. Aperiam consequuntetur adipisicing elit. Aperiam consequuntetur adipisicing elit. Aperiam consequuntetur adipisicing elit. Aperiam consequuntetur adipisicing elit. Aperiam consequuntetur adipisicing elit. Aperiam consequuntetur adipisicing elit. Aperiam consequuntetur adipisicing elit. Aperiam consequuntur cum debitis dolore iure officia pariatur reprehenderit saepe tenetur veritatis. Autem beatae culpa distinctio facere id minima natus neque optio!</p>
            </section>
        </section>
    </section>
</section>
<!-- start main body -->

<!-- footer -->

<section class="container-fluid ">
    <section class="row ">
        <section class=" col-8 offset-2 mt-3">
            <footer class="text-dark text-center">
                <hr>
                <p>Copyright &copy; 2019 Blessing.com<p>
            </footer>
        </section>
    </section>
</section>
<!-- end footer -->
<!-- locate js -->
<script src="front/js/jquery-3.3.1.slim.min.js"></script>
<script src="front/js/bootstrap.min.js"></script>
<script src="front/js/popper.min.js"></script>
<script src="front/js/icon.js"></script>
</body>
</html>