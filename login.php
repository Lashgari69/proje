<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <link rel="stylesheet" href="front/css/bootstrap.min.css">
    <link rel="stylesheet" href="front/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="front/css/custom.css">
    <title>صفحه ورود</title>
</head>
<body class="httm">
<section class="container-fluid">
    <section class="row">
        <section class="col-8 offset-2 mt-5">
            <header class="text-center text-primary">
               <h1>Blessing</h1>
            </header>
        </section>
    </section>
</section>
<section class="container-fluid">
    <section class="content">
    <section class="row">
        <section class="col-8 offset-2 mt-3 ">
            <section class=" jumbotron jumcolor ">
                <section class="text-right">
            <form action="" method="">
                <input type="hidden" name="order" value="login">
                <section class="form-group ">
                <label for="username"><h5>نام کاربری </h5></label>
                <input type="text" id="username" name="username" class="form-control pr-3" required="required">
                </section>
                <section class="form-group">
                    <label for="password"><h5>رمز عبور </h5></label>
                <input type="text" id="password" name="password" class="form-control pr-3" required="required">
                </section>
                <section class="form-group">
                    <img  src="gd.php"  class="d-inline-flex" />
                    <label for="password" class="d-inline-flex"><h5>کد را وارد کنید </h5></label>
                    <input type="text" name="code" class="form-control " required="required">
                </section>
                 <button type="submit" class="btn btn-primary btn-block">ورود</button>
                <section class=" mt-4 text-center">
                    <p>کاربر جدید هستید؟ <a href="register.php">ثبت نام</a> </p>
                </section>

            </form>
            </section>
            </section>
        </section>
    </section>
    </section>
</section>
<hr>
<section class="container-fluid ">
    <section class="row ">
        <section class=" col-8 offset-2 mt-3">
            <footer class="text-dark text-center">
                <p>Copyright &copy; 2019 Blessing.com<p>
            </footer>
        </section>
    </section>
</section>
<!-- locate js -->
<script src="front/js/jquery-3.3.1.slim.min.js"></script>
<script src="front/js/bootstrap.min.js"></script>
</body>
</html>